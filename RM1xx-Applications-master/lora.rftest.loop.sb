//******************************************************************************
// Copyright (c) 2016-2017, Laird
//
// Permission to use, copy, modify, and/or distribute this software for any
// purpose with or without fee is hereby granted, provided that the above
// copyright notice and this permission notice appear in all copies.
//
// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
// WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
// SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
// WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
// ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR
// IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
//
// SPDX-License-Identifier:ISC
//
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// +++++                                                                      ++
// +++++  When UwTerminal downloads the app it will store it as a filename    ++
// +++++  which consists of all characters up to the first . and excluding it ++
// +++++                                                                      ++
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
// This application allows for testing LoRa RF functionality
//
//******************************************************************************
#include "RM1xx-defs.h"

        //Size of i[]
#define NUM_OF_I_PARAMS                     (8)
        //Size of s$[] and must be at least 8 elements for BleSvcRegDevInfo()
#define NUM_OF_S_PARAMS                     (4)
#define NUM_DURATION                     (5000) //5000 ms, 5 detik
#define NUM_ADC_DATA_QUEUE                (120) //jumlah buffer adc data
#define SEND_DELAY_TRY                     (5)
#define ACK_TIMEOUT_MAX                    (10)
#define MAX_NODE (2)
#define PACKET_JOIN  (0)
#define PACKET_DATA  (1)

#define STATE_START    (0)
#define STATE_JOINING  (1) //joining
#define STATE_WAIT_JOIN_RESPONSE (2) //waiting join response
#define STATE_JOIN_TIMEOUT (3)
#define STATE_JOINED   (4) //joined
#define STATE_SENDING_DATA (5) //sending data
#define STATE_SENDING_DELAY (6)
#define STATE_SENDBUF_OK (7)
#define STATE_SEND_COMPLETE (8)
#define STATE_WAIT_ACK (9)  //waiting ack
#define STATE_ACK_TIMEOUT (10)
#define STATE_SENT_SUCCESS_NO_RTO (11)
#define STATE_IDDLE (12)

//-------------------------------------------------------------------------------------------------------//
//-------------------------------------------------------------------------------------------------------//

dim dp
dim looprc                         //
dim PrevAds$[8]                    //Previous adverts (size of 8 adverts)
dim PrevAdCount : PrevAdCount = 0  //Number of previous adverts
dim PacketLoRa$[8] as string                //Number of Packet Sent to LoRa
 
Dim rc
Dim Count
dim str$
dim strResp$
dim Power$
dim Datarate$
dim tempIdx
dim val
dim myNum
dim State
dim secondCnt
dim delayCnt: delayCnt = 0
dim delayAck: delayAck = 0
dim dataIdx : dataIdx = 0
dim dataInIdx: dataInIdx = 0
dim dataSentIdx: dataSentIdx = 0

dim nodesValue$[NUM_ADC_DATA_QUEUE] as string

//Peripheral address registered
dim nodeAddrHex[8] as string

//rtd 1 01D053264B82AC
//rtd 2 01C6D83A568658
nodeAddrHex[0] = "01D053264B82AC"
nodeAddrHex[1] = "01C6D83A568658"
//nodeAddrHex[2] = "01CF5067750838"
//nodeAddrHex[2] = "01CF5067750839"
//nodeAddrHex[2] = "01CF5067750840"

//==============================================================================
// Prints 
//==============================================================================
SUB PrintMsg(str$)
    rc = UartWrite(str$)
ENDSUB 

SUB PrintLine()
    str$ = "\n---------------------------------------------------------\n\n"
    //PrintMsg(str$)
ENDSUB

SUB PrintSendParameters()
    rc = LORAMACGetOption(LORAMAC_OPT_DATA_RATE, Datarate$)
    rc = LORAMACGetOption(LORAMAC_OPT_TX_POWER, Power$)
    //sPrint #str$,"\npower: ";Power$
    //PrintMsg(str$)
    //sPrint #str$,"\ndatarate: ";Datarate$
    //PrintMsg(str$)
ENDSUB

SUB PrintNextTxTime()
    rc = LORAMACGetOption(LORAMAC_OPT_NEXT_TX, strResp$)
    //sPrint #str$,"\n";strResp$;
    //PrintMsg(str$)
ENDSUB

//-------------------------------------------------------------------------------------------------------//
//-------------------------------------------------------------------------------------------------------//
FUNCTION LoramacJoining() As Integer
    str$ = "\nJoining\n"
    PrintMsg(str$)
    State = STATE_JOINING
endfunc 1

//-------------------------------------------------------------------------------------------------------//
//-------------------------------------------------------------------------------------------------------//
FUNCTION LoramacJoined() As Integer
    str$ = "\nJoined\n"
    PrintMsg(str$)
    //str$ = "0"
    //rc = LoramacSetOption(LORAMAC_OPT_ADR_ENABLE,str$)
	str$ = "3"
    rc = LORAMACSetOption(LORAMAC_OPT_DATA_RATE,str$)  //use lowest data rate
    State = STATE_JOINED
endfunc 1

//-------------------------------------------------------------------------------------------------------//
//-------------------------------------------------------------------------------------------------------//
FUNCTION LoramacTxComplete() As Integer
    str$ = "\nTx sequence completed\r\n"
    //PrintMsg(str$)
	
	SELECT State
		case STATE_JOINING
		     //Print "OTX STATE_JOINING----------------\r\n"
		     State = STATE_WAIT_JOIN_RESPONSE
		case STATE_JOINED
		     //Print "OTX STATE_JOINED----------------\r\n"
		     //State = STATE_JOINED
		case STATE_SENDING_DATA
		     //Print "OTX STATE_SENDING_DATA----------------\r\n"
		     State = STATE_SEND_COMPLETE
		case STATE_SENDBUF_OK
		     //Print "OTX STATE_SENDBUF_OK----------------\r\n"
             state = STATE_SEND_COMPLETE		
	    case STATE_SEND_COMPLETE
		     //Print "OTX STATE_SEND_COMPLETE----------------\r\n"
             State = STATE_WAIT_ACK		
		case else
		     //Print "OTX ELSE----------------\r\n"
		     //State = STATE_IDDLE
	ENDSELECT
	
	//Start Timer
	//TimerStart(0,500,0) //kirim lagi, 1/2 detik
endfunc 1
//-------------------------------------------------------------------------------------------------------//
//-------------------------------------------------------------------------------------------------------//

// FUNCTION HandlerRxData() As Integer
    // dim datastr$ as string
    // dim rssi$ as integer
    // dim port$ as integer
    // dim snr$ as integer
    // dim framepending$ as integer
    // dim packettype$ as integer
    // rc = LORAMACRxData(datastr$,rssi$,port$,snr$,framepending$,packettype$)
    // //sPrint #str$,"\n\22";datastr$;"\22 received from the gateway"
    // Print str$
    // //sPrint #str$,"\nReceived packet rssi: ";rssi$;" snr: ";snr$;" frames pending: ";framepending$;" packet type: ";packettype$
    // Print str$
// endfunc 1

//-------------------------------------------------------------------------------------------------------//
//-------------------------------------------------------------------------------------------------------//

SUB InitLoRa()
	//rc=LORAMACJoin(LORAMAC_JOIN_BY_PERSONALIZATION)
	rc=LORAMACJoin(LORAMAC_JOIN_BY_REQUEST)

	//Send an OK response
	//Print "Init Lora finish\r\n"

    //Start Timer0
    //TimerStart(0,500,0)
ENDSUB	

//-------------------------------------------------------------------------------------------------------//
//-------------------------------------------------------------------------------------------------------//

FUNCTION LoramacRxTimeout() As Integer
    str$ = "\nRx Timeout\n"
    //PrintMsg(str$)
    //PrintLine()
	
	SELECT State
		Case STATE_JOINING
		   //Print "LRO STATE_SENDING_DATA\r\n"
	       State =  STATE_JOIN_TIMEOUT
		case STATE_WAIT_JOIN_RESPONSE
		   //Print "LRO STATE_WAIT_JOIN_RESPONSE\r\n"
           State = STATE_JOIN_TIMEOUT		
		Case STATE_SENDING_DATA
		   //Print "LRO STATE_SENDING_DATA\r\n"
		   State = STATE_ACK_TIMEOUT
		Case STATE_SEND_COMPLETE
		   //Print "LRO STATE_SEND_COMPLETE\r\n"
           state = STATE_ACK_TIMEOUT		
		Case STATE_WAIT_ACK
		   //Print "LRO STATE_WAIT_ACK\r\n"
		   State = STATE_ACK_TIMEOUT
		case STATE_SENDBUF_OK
            //Print "LRO SENDBUF_OK"
			state = STATE_ACK_TIMEOUT
		Case else
		   //Print "LRO ELSE\r\n"
           //State = STATE_IDDLE		
	ENDSELECT
    //InitLoRa()
endfunc 1

//-------------------------------------------------------------------------------------------------------//
//-------------------------------------------------------------------------------------------------------//

FUNCTION LoramacTxTimeout() As Integer
    str$ = "\nTx Timeout\n"
    //PrintMsg(str$)
    //PrintLine()
	
	SELECT State
		CASE STATE_JOINING
		   //Print "LTO STATE_JOINING\r\n"
	       //State =  STATE_JOINING
		CASE STATE_SENDING_DATA
		   //Print "LTO STATE_SENDING_DATA\r\n"
		   //State = STATE_SENDING_DATA
		CASE ELSE
		   //Print "LTO ELSE\r\n"
           //State = STATE_IDDLE		
	ENDSELECT
	//InitLoRa()
endfunc 1

//-------------------------------------------------------------------------------------------------------//
//-------------------------------------------------------------------------------------------------------//

FUNCTION LoramacRxError() As Integer
    str$ = "\nRx Error\n"
    //PrintMsg(str$)
    //PrintLine()
	//InitLoRa()
endfunc 1

//-------------------------------------------------------------------------------------------------------//
//-------------------------------------------------------------------------------------------------------//

FUNCTION LoramacLinkResponse(Margin, Gateways) As Integer
    //sPrint #str$,"\nMargin: ";Margin
    //PrintMsg(str$)
endfunc 1

//-------------------------------------------------------------------------------------------------------//
//-------------------------------------------------------------------------------------------------------//

FUNCTION HandlerNextTx() As Integer
    str$ = "\nNextTx"
    //PrintMsg(str$)
endfunc 1

//------------------------------------------------------------------------------
// Uplnk/downlink sequence has completed - this is an amalgamation of the above events.
// The flag indicates which of the above end of seqnece events triggered this event.
// nexttime is the time to the next EVLORAMACNEXTTX  event.
// For AU and US modules this will be 0 and it is safe to send the next packet.
//------------------------------------------------------------------------------
FUNCTION HandlerSequenceComplete(flag, nexttime) As Integer
    Print  "\nSequence complete ";flag
    Print  "\nNext time ",nexttime
    //PrintLine()
endfunc 1

//*******************************************************************************
//*******************************************************************************
function HandlerTimer0() as integer
	dim tmp
	dim currentsize
	dim maxsize
	dim header$ as string
	dim adcStr$ as string
	
	secondCnt = secondCnt + 1
	
	SELECT secondCnt
        Case 1 //detik pertama
		    //Print "timer : 1 second\r\n"
		    if state >= STATE_JOINED then //jika sudah joined 
			  Print "1 menit occur"
			  //klo udah joined 
			  //get data adc from 2 sensors
			  //S//Print #header$,integer.h' dataInIdx
			  SPrint #header$,integer.h' myNum
			  header$ = RIGHT$(header$,3)
			  //Print "header:"
			  //Print header$
			  //Print "\r\n"
			  //PacketLoRa$[0] = "03FF"
			  //PacketLoRa$[1] = "027E"
			  header$ = "58"
			  adcStr$ = LEFT$(PacketLoRa$[0],4)
			  adcStr$ = RIGHT$(adcStr$,3)
			  nodesValue$[dataIdx] = header$ + adcStr$ //LEFT$(PacketLoRa$[0],4) + LEFT$(PacketLoRa$[1],4) //"03FF"+ "027E" 
			  //Print "idx="
			  //Print nodesValue$[dataIdx]
			  //Print "\r\n"
			  dataInIdx = dataInIdx + 1 //increment index
			  dataIdx = dataIdx + 1
			  myNum = myNum + 1
			  if myNum >= 60 then
			     myNum = 0
			  endif	 
		    endif	
        Case 60 //sdh 1 menit, restart, batas timeout ack sdh diterima
		    secondCnt = 0
			//Print "timer reset counter timer++++++++++++++++\r\n"
        case else
		    //Print "sec:"
			//Print secondCnt
		    //Print "\r\n"
    Endselect		
	
	SELECT state
	    case STATE_START
			 //Print "timer STATE_START\r\n"
		     State = STATE_JOINING
		Case STATE_JOINING 
			 //Print "timer STATE_JOINING\r\n"		
			 State = STATE_WAIT_JOIN_RESPONSE
		//Case STATE_WAIT_JOIN_RESPONSE 
			 //Print "timer STATE_WAIT_JOIN_RESPONSE\r\n"
		     //State = STATE_WAIT_JOIN_RESPONSE
		Case STATE_JOINED   
 		     //Print "timer STATE_JOINED\r\n"
		     State = STATE_SENDING_DATA
		Case STATE_SENDING_DATA 
			 //Print "timer STATE_SENDING_DATA\r\n"
			 if dataInIdx > 0 then //jika sudah ada data dari adc
				rc = LoramacQueryTxPossible(strlen(nodesValue$[0]),currentsize,maxsize)
				if rc ==0 then
				   //Print "timer send data query ok:\r\n"
				   rc = LORAMACTxData(2,nodesValue$[0], 1) //send with request ACK
				   //Print nodesValue$[0]
				   //Print "\r\n"
				   if  rc == 0 then
					   PrintNextTxTime() //successfully
					   State = STATE_SENDBUF_OK
					   Print "Sending OK\r\n"
				   else
				       //Print "timer sendTx fail\r\n"
			           state = STATE_SENDING_DELAY
				   endif
			    else
			       state = STATE_SENDING_DELAY
				   Print "Packet too large\n"
				   Print "size ";strlen(nodesValue$[0]);" max ";currentsize;"\n"
				   Print "maxsize : ";maxsize;"\n"
			    endif		
			 endif
	    case STATE_SENDING_DELAY
		     delayCnt = delayCnt + 1
			 if delayCnt >= SEND_DELAY_TRY then
			    delayCnt = 0
				state = STATE_SENDING_DATA		
			 endif
		case STATE_SENDBUF_OK
			 //Print "timer STATE_SENDBUF_OK\r\n"
             //state = STATE_SENDBUF_OK		
        case STATE_SEND_COMPLETE //tx sent ok
		     //Print "timer STATE_SEND_COMPLETE\r\n"
             state = STATE_WAIT_ACK 		
		Case STATE_WAIT_ACK 
		     //Print "timer STATE_WAIT_ACK\r\n"
		     //State = STATE_DELAY_ACK
			 delayAck = delayAck + 1
			 if delayAck >= ACK_TIMEOUT_MAX then
				delayAck = 0
				state = STATE_SENT_SUCCESS_NO_RTO
			 endif 
		Case STATE_JOIN_TIMEOUT // join lg
			 //Print "timer STATE_JOIN_TIMEOUT\r\n"
			 InitLoRa()
             state = STATE_JOINING		
		Case STATE_ACK_TIMEOUT  //kirim data lagi
		     //Print "timer STATE_ACK_TIMEOUT\r\n"
		     state = STATE_SENDING_DATA
		case STATE_SENT_SUCCESS_NO_RTO
			 //Print "timer STATE_SENT_SUCCESS_NO_RTO\r\n"
			 Print "Sent\r\n"
			 if state == STATE_SENT_SUCCESS_NO_RTO then
				  //geser fifo
				  dataIdx = dataIdx - 1
				  dataInIdx = dataInIdx - 1 
			      for tmp = 0 to dataInIdx
	                  nodesValue$[tmp] = nodesValue$[tmp+1]	
					  //Print "data:"
					  //Print tmp
					  //Print "="
					  //Print nodesValue$[tmp]
					  //Print "\r\n"
			       next
				   state = STATE_IDDLE
			  endif 
              state = STATE_IDDLE
        case STATE_IDDLE
			 //Print "timer STATE_IDDLE:"
             //cek index data masuk, klo kosong iddle lg, klo ad kirimkan
			 if dataInIdx >= 0 then
			    //Print dataInIdx
			    state = STATE_SENDING_DATA
				//Print "\r\n"
             endif			 
		case else 
		     //state = STATE_IDDLE
	ENDSELECT
endfunc 1

//This function removes leading 0s from the front of a string
FUNCTION RemoveZeros(Data$) AS STRING
    dim i, Done, TmpStr$ : i = 0 : Done = 0
    WHILE (i < strlen(Data$))
        TmpStr$ = MID$(Data$, i, 1)
        IF (STRCMP(TmpStr$, "0") != 0) THEN
            //Other number found - cut string to this length and mark as finished
            TmpStr$ = RIGHT$(Data$, strlen(Data$)-i)
            i = strlen(Data$)
            Done = 1
        ENDIF
        i = i+1
    ENDWHILE

    IF (Done == 0) THEN
        //Other number not found in provided string
        TmpStr$ = "0"
    ENDIF
ENDFUNC TmpStr$

// //*******************************************************************************
// //*******************************************************************************
// //This handler will be called when an advert report is received
FUNCTION HndlrAdvRpt()
   DIM periphAddr$, advData$, nRssi, ADval$, TmpStr$, TmpStr2$, TmpVal,temp2$, nodeIdx, length

	//tempIdx = 37
    //Read all cached advert reports
    looprc=BleScanGetAdvReport(periphAddr$, advData$, TmpVal, nRssi)
    WHILE (looprc == 0) //wait until adv received successfully
		//Check if this advert was received recently
		TmpStr$ = periphAddr$ + advData$
		TmpVal = 0 //data baru
		while (TmpVal < PrevAdCount) //data baru, bukan ambil sisa dari cache 
			//if (strcmp(TmpStr$, PrevAds$[TmpVal]) == 0) then //bandingkan addressnya
				//Already seen this advert, ignore it
			//	TmpVal = 254
			//endif
			//temp1$ = "01ED4C0C0C6CF1"
			temp2$ = strhexize$(periphAddr$)
			
			//Get node index 
			FOR nodeIdx = 0 to (MAX_NODE-1)
			    //tempIdx = MAX_NODE + 2
			    if (strcmp(nodeAddrHex[nodeIdx], temp2$) == 0) then //bandingkan addressnya
					//Already seen this advert, ignore it
					//TmpVal = 254
					//Print "\r\n...matching...registered\n"
					tempIdx = nodeIdx
					BREAK
				endif
			NEXT
			
			TmpVal = TmpVal+1
		endwhile

		//if (TmpVal != 255) then //klo 255 berarti datanya sama dg data lama
		    //nodeIdx = MAX_NODE + 2
			//Print "tempIdx:";tempIdx;"\n"
		    //if(tempIdx != 37) then //klo peripheral address teregister sj
				//Output BT addr and RSSI
				//Print strhexize$(periphAddr$);", RSSI: ";nRssi;"\n"
				
				//Go through all tags and //Print them out
				//Print "Advertising data cuk[hex]:\n"
				TmpVal = 0
				while (TmpVal <= 0xff) //klo data di antara 0-255
					if (TmpVal == 8) then //klo udah 8
						//Skip name tags
						TmpVal = 10
					endif
			
				    //if(tempIdx == nodeIdx) then
						rc = BLEGETADBYTAG(advData$, TmpVal, TmpStr$)
						if (rc == 0) then
							//Output tag
							TmpStr2$ = ""
							SPrint #TmpStr2$,INTEGER.H'TmpVal
							TmpStr2$ = RemoveZeros(TmpStr2$)
							length = strlen(TmpStr$)
							//Print "  Length: ";strlen(TmpStr$);", Type: 0x";TmpStr2$;", Value: ";STRHEXIZE$(TmpStr$);"\n"
							//klo typenya data ADC
							if(TmpVal == 0xFF) then	
								//if(tempIdx == nodeIdx) then
								    if length < 3 then
										PacketLoRa$[tempIdx] = STRHEXIZE$(TmpStr$)
										//Print "  Packet:";tempIdx;",Data:";PacketLoRa$[tempIdx];"\n"
										//Print "\nRtd 0 value:";PacketLoRa$[0];"\r\n"
										//Print "\nRtd 1 value:";PacketLoRa$[1];"\r\n"
										////Print "\nPacket Lora2:";PacketLoRa$[2];"\r\n"
									endif
								//endif   
							endif
						endif	
					//endif	
					TmpVal = TmpVal+1
				endwhile

				//Newline
				//Print "\n"

				//Check if the array is too large
				if (PrevAdCount > 6) then
					//Array too big - clear it
					TmpVal = 0
					while (TmpVal < 8)
						PrevAds$[TmpVal] = ""
						TmpVal = TmpVal+1
					endwhile
					PrevAdCount = 0
				endif

				//Add this adverts to the recent list
				TmpStr$ = periphAddr$ + advData$
				PrevAds$[PrevAdCount] = TmpStr$
				PrevAdCount = PrevAdCount+1
			//endif	
		//endif
			looprc=BleScanGetAdvReport(periphAddr$, advData$, TmpVal, nRssi) //ambil data baru lagi
    ENDWHILE
ENDFUNC 1
//*******************************************************************************
//*******************************************************************************

//Register events BLE
ONEVENT EVBLE_ADV_REPORT   CALL HndlrAdvRpt //Called when an advert report is received

//Register events LORA
ONEVENT EVLORAMACJOINING    CALL LoramacJoining
ONEVENT EVLORAMACJOINED    CALL LoramacJoined
ONEVENT EVLORAMACTXCOMPLETE    CALL LoramacTxComplete
ONEVENT EVLORAMACRXTIMEOUT CALL LoramacRxTimeout
ONEVENT EVLORAMACTXTIMEOUT CALL LoramacTxTimeout
ONEVENT EVLORAMACRXERROR CALL LoramacRxError
ONEVENT EVLORAMACLINKCHECKRESPMSG CALL LoramacLinkResponse
ONEVENT EVLORAMACSEQUENCECOMPLETE CALL HandlerSequenceComplete
ONEVENT EVLORAMACNEXTTX CALL HandlerNextTx
//ONEVENT EVLORAMACRXDATA CALL HandlerRxData
ONEVENT EVTMR0    CALL HandlerTimer0

//==============================================================================
//==============================================================================
sub Startup()
	dp=4 : rc = BleTxPowerSet(dp)
	//Print "\nrc = ";rc
	//Print "\nTx power : desired= ";dp," actual= "; SysInfo(2008)

	dp=4 : rc = BleTxPwrWhilePairing(dp)
	//Print "\nrc = ";rc
	//Print "\nTx power while pairing: desired= ";dp," actual= "; SysInfo(2018)

	//Start scanning
	rc = BLESCANCONFIG(3, 4) //Change cache size to 4 reports
	rc = BLESCANCONFIG(2, 1) //Active scanning
	rc = BleScanStart(0, 0)  //Scan with no timeout
	
	//Send an OK response
	//Print "Init BLE ADV Ready\r\n"
endsub

//******************************************************************************
// Equivalent to main() in C
//******************************************************************************
Startup()
InitLoRa()

//Send an OK response
Print "Start\r\n"

myNum = 0
state = STATE_START
TimerStart(0,1000,1) //Timer 1 detik, continuous

WAITEVENT